/**
 * 
 */
package to.lookup.logger;

/**
 * @author vinodbokare
 *
 */
import static org.elasticsearch.node.NodeBuilder.*;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequest;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.cursors.ObjectObjectCursor;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;
import static org.elasticsearch.common.xcontent.XContentFactory.*;
public class Client1_7 {

	private static final String INDEX_NAME = "locallogspfnew";
	private static final String INDEX_TYPE = "order";
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		System.out.println("Started 1.7");
		
		
		// on startup

		Node node = nodeBuilder().node();
		Client client = new TransportClient()
		        .addTransportAddress(new InetSocketTransportAddress("localhost", 9300))
		        .addTransportAddress(new InetSocketTransportAddress("127.0.0.1", 9300));

		getAllIndexes(client);
		
		//GET INDEX
		String jsonPayLoad = getIndex(client, "locallogspf", "order", "1");
				
		
		//Create Index
		createIndex(client,INDEX_NAME,INDEX_TYPE,jsonPayLoad);

		
		//Delete Index
		//deleteIndex(client,INDEX_NAME,INDEX_TYPE);
		
	}
	
	//GET INDEX
	
	public static String getIndex(Client client,String indexName,String indexType,String id){
		
		GetResponse logPfResponse = client.prepareGet(indexName, indexType, id)
		        .execute()
		        .actionGet();
		
		return logPfResponse != null ? logPfResponse.getSourceAsString(): null;
	}

	
	//Get All Indexes
	
	public static void getAllIndexes(Client client){
		
		 GetMappingsResponse res;
		try {
			res = client.admin().indices().getMappings(new GetMappingsRequest().indices("locallogspf")).get();
			  ImmutableOpenMap<String, MappingMetaData> mapping  = res.mappings().get("locallogspf");
			    for (ObjectObjectCursor<String, MappingMetaData> c : mapping) {
			    	System.out.println("################################################");
			        System.out.println(c.key+" = "+c.value.source());
			    }
			    
			    
			    
			    GetMappingsResponse response = client
			    		.admin()
			            .indices()
			            .prepareGetMappings()
			            .execute()
			            .actionGet();

			        ImmutableOpenMap<String, ImmutableOpenMap<String, MappingMetaData>> mappings = response.getMappings();
   
			    for( ObjectObjectCursor<String, ImmutableOpenMap<String, MappingMetaData>> c :mappings ){
			    	System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			    	
			    	for(ObjectObjectCursor<String, MappingMetaData> x : c.value){
			    		  System.out.println(c.key+" = "+x.value.source());
			    	}
			    //    System.out.println(c.key+" = "+c.value.keys()+ "::"+c.value.values().toString());
			    }
			        
			        
			        
			        
			        
			    
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	}
	
	
	
	//Create INDEX
	
	public static void createIndex(Client client,String indexName,String indexType,String jsonPayload){
		try {
			IndexResponse response = client.prepareIndex(indexName, indexType, UUID.randomUUID().toString())
			        .setSource(jsonPayload)
			        .execute()
			        .actionGet();
		} catch (ElasticsearchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	//Update API	
	
	
	
	
   //DELETE API
	
	private static void deleteIndex(Client client, String indexName, String indexType) {

		DeleteResponse response = client.prepareDelete(indexName, indexType, "1")
		        .execute()
		        .actionGet();
	}
	
	
	
	
	public static void createIndex(Client client){
		try {
			IndexResponse response = client.prepareIndex("twitter", "tweet", "1")
			        .setSource(jsonBuilder()
			                    .startObject()
			                        .field("user", "kimchy")
			                        .field("postDate", new Date())
			                        .field("message", "trying out Elasticsearch")
			                    .endObject()
			                  )
			        .execute()
			        .actionGet();
		} catch (ElasticsearchException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
