/**
 * 
 */
package to.lookup.logger;


import java.io.IOException;
/**
 * @author vinodbokare
 *
 */
import java.net.InetAddress;
import java.util.Date;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentFactory;

import static org.elasticsearch.common.xcontent.XContentFactory.*;

public class NewClient {

	public static void main(String[] args) {

	    Client client = new TransportClient()
	    .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));

	    IndexResponse response = null;
	    try {
	      response = client.prepareIndex("twitter", "tweet", "1")
	          .setSource(XContentFactory.jsonBuilder()
	                      .startObject()
	                          .field("user", "john")
	                          .field("postDate", new Date())
	                          .field("message", "who dont it work")
	                      .endObject()
	                    )
	          .execute()
	          .actionGet();
	    } catch (ElasticsearchException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }

	    System.out.println(response);
	}
}
